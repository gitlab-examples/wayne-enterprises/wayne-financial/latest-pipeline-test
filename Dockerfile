FROM python:alpine

RUN apk update && apk upgrade && apk add --no-cache musl-dev=1.2.3-r4 gcc=12.2.1_git20220924-r4 sqlite-dev=3.40.1-r0 && apk add --no-cache --repository http://dl-cdn.alpinelinux.org/alpine/v3.16/main/ mariadb=10.6.12-r0 mariadb-client=10.6.12-r0 && apk add --no-cache mariadb-connector-c-dev